# Gitlab issues label classification

Prototype for classifying labels automatically to Gitlab issues via deep learning. **WIP**

After I decided to kill some time by analyze some data that I can get from [Gitlab.com](https://www.gitlab.com) through the [API](https://docs.gitlab.com/ee/api/) I realized I could build something top of the massive amount of available text that comes from issues. Gitlab CE and EE issue labels seemed consistent so with deep learning we can predict these labels easily for both projects. Gotcha, I found a related issue https://gitlab.com/gitlab-org/gitlab-ce/issues/19369 as well.

The project is based on this [Jupyter notebook](https://gitlab.com/tromika/fun-with-gitlab-api/blob/master/notebooks/gitlab-issues-labels-classification.ipynb) originally. So actually this the production deployment of the experimented model from the notebook.

You can find the metrics of the model on [comet.ml](https://www.comet.ml/metricbrew/gitlab-issue-label-cf).


The project consists of a model and a server:
* A model trained on issues from Gitlab EE and CE projects. Using a quite simple [tensorflow](https://www.tensorflow.org/) deep learning model.
* A prediction API that can classify issue labels in real time. Now it's based on Flask but at some point it would be great to move to [tensorflow serving](https://www.tensorflow.org/serving/)

You can try it out on easily with navigating your browser to the following link. (Please note it's using project and issue ids as parameters. You can find the issue id in the issue url `https://gitlab.com/gitlab-org/gitlab-ce/issues/19369` and here are the project ids: `Gitlab CE: 13083`, `Gitlab EE: 278964`)


[http://gitlab.metricbrew.com:5000/predict?project=13083&issue=19369](http://gitlab.metricbrew.com:5000/predict?project=13083&issue=19369)


The response is a JSON object as follows:

`{"labels":{"Platform":"0.05","feature_proposal":"0.62","Plan":"0.22"},"model_used_for_prediction":"97cea30990a04ad0877b8dec2830a3a3","success":true}`

The object is containing the top 3 labels with the highest likelihood. The format is a list of class label/probability pairs.


Right now the model is trained on the following list of labels so it can only classify these through the API:

['feature proposal', 'bug', 'frontend','Create', 'Platform', 'Plan', 'CI/CD','UX',  'backend', 'customer',
'regression', 'api', 'performance' ,'Geo', 'database', 'UI polish', 'devops:create', 'Manage', 'Distribution', 'Quality',
'import', 'Monitoring','potential proposal', 'Release','Packaging','security','customer+', 'authentication','boards',
'admin dashboard',  'Security Products','container registry', 'webhooks', 'devops:release',
'Gitaly','ci-build', 'kubernetes','oos', 'Secure',  'installation', 'Configuration', 'Monitoring']



## Data

The data is exported from [Gitlab.com](https://www.gitlab.com) through the [API](https://docs.gitlab.com/ee/api/). Actually in the other project you find a notebook where you can check how you can export it by yourself but as it may take a while in the data folder you can find the data I used for the model.

## Data Preparation

* Markdown components are removed
* Issue template words are excluded
* Stopwords are also removed with some other words removed: {'ok', 'head', 'ago', 'author1', 'n', 'days', 'yes', 'no', 'hours', 'description', 'summary'}

For the prediction it's using the **title** and **description** strings joined together.

## Workflow

With `pipeline.py` you can download all the data you need, clean it, train the model and finally upload the model to Google Storage.

For the prediction api you can use the Docker image from the registry or the app under serving-app

## Install

This project requires **Python** and the following Python libraries installed:

- [Pandas](http://pandas.pydata.org/)
- [flask](http://flask.pocoo.org/)
- [scikit-learn](http://scikit-learn.org/stable/)
- [keras](https://keras.io/)
- [tensorflow](http://tensorflow.org/)
- [spacy](https://spacy.io/)
- [gcloud](https://cloud.google.com/python/)

## docker

The flask based api is also available in the project's registry.
