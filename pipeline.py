import pandas as pd
import pickle
import os, sys
import numpy as np
import gc
import logging


from features.feature_engineering import Features
from models.deeplearning import KerasModel
from data.get_input_data import InputData

import tensorflow as tf
from keras import backend as K
from comet_ml import Experiment

from tensorflow.python.saved_model import builder as saved_model_builder
from tensorflow.python.saved_model import tag_constants, signature_constants, signature_def_utils_impl

from utils.utils import get_args, get_config, upload_to_gcloud

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)


def main():
    try:
        args = get_args()
        config = get_config()
    except Exception as e:
        print("missing or invalid arguments %s" % e)
        exit(0)
    if args.re_download_data:
        #Still hardcoded
        input_data = InputData(config, [13083, 278964])
        train_data = input_data.download_input_data()

    experiment = Experiment(api_key=config['COMET_ML_TOKEN'],
                            project_name="gitlab-issue-label-cf",
                            workspace="metricbrew",
                            log_git_metadata=True)
    #Comet url https://www.comet.ml/metricbrew/gitlab-issue-label-classification
    feature_builder = Features(config, experiment)
    data = feature_builder.get_train_test_data()

    session = tf.Session()
    K.set_session(session)
    K.set_learning_phase(0)
    params={
            "batch_size": 32,
            "epochs": 1,
            "num_classes": feature_builder.num_classes,
            "max_words" : feature_builder.max_words,
            "optimizer": 'adam',
            "loss_func" : 'categorical_crossentropy',
            "metric" : ['accuracy', 'top_k_categorical_accuracy'],
            "dropout": 0.5,
            "validation_split" : 0.2}

    experiment.log_multiple_params(params)
    keras_model = KerasModel(params, experiment)
    # Build the network
    model = keras_model.build_model()
    logger.info("Training the model")
    with experiment.train():
        history = model.fit(data['X_train'],
                            data['Y_train'],
                            batch_size = params['batch_size'],
                            epochs = params['epochs'],
                            verbose = 1,
                            validation_split = params['validation_split'])

    # Evaluate the accuracy of our trained model
    logger.info("Testing the model")
    with experiment.test():
        loss, accuracy, top_k_categorical_accuracy = model.evaluate(data['X_test'],
                               data['Y_test'],
                               verbose=1)
        metrics = {
          'loss':loss,
          'accuracy':accuracy,
          'top_k_categorical_accuracy' : top_k_categorical_accuracy
        }
        experiment.log_multiple_metrics(metrics)
        logger.info('loss: '.format(loss))
        logger.info('accuracy: '.format(accuracy))
        logger.info('top_k_categorical_accuracy: '.format(top_k_categorical_accuracy))
    experiment.log_other("comments", "Pipeline run")
    logger.info("Saving the model")

    # Upload the model and deps to Google Storage

    filename = "keras_"+str(experiment.get_key())+".h5"
    path = os.path.join("models", "storage")
    model.save(os.path.join(path, filename))
    upload_to_gcloud(path, filename)

    path = os.path.join("models","vocabulary")
    filename = "tokenizer_"+str(experiment.get_key())+".pkl"
    upload_to_gcloud(path, filename, config)

    path = os.path.join("models","encoder")
    filename = "encoder_"+str(experiment.get_key())+".pkl"
    upload_to_gcloud(path, filename, config)

if __name__ == '__main__':
    main()
