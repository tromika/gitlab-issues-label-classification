from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from typing import Any, Dict
import logging, sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)


class KerasModel():
    def __init__(self, params : Dict[str, Any]):
        self.batch_size = params['batch_size']
        self.epochs = params['epochs']
        self.loss_function = params['loss_func']
        self.optimizer = params['optimizer']
        self.metric = params['metric']
        self.dropout_rate = params['dropout']
        self.validation_split = params['validation_split']
        self.max_words = params['max_words']
        self.num_classes = params['num_classes']

    def build_model(self) -> Sequential:
        logger.info('Compiling the model')
        # Build the model
        model = Sequential()
        model.add(Dense(512, input_shape=(self.max_words,)))
        model.add(Activation('relu'))
        model.add(Dropout(self.dropout_rate))
        model.add(Dense(self.num_classes))
        model.add(Activation('softmax'))

        model.compile(loss=self.loss_function,
                      optimizer=self.optimizer,
                      metrics=self.metric)
        return model
