import argparse, os
from gcloud import storage
from oauth2client.service_account import ServiceAccountCredentials


def get_args():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("-r",
                        "--re_download_data",
                        help="Re-download data from gitlab")
    args = argparser.parse_args()
    return args

def get_config():
    config = {}
    if all( var in os.environ for var in ['GCLOUD_CLIENT_ID', 'GCLOUD_CLIENT_EMAIL', 'GCLOUD_PRIVATE_KEY_ID', 'GCLOUD_PRIVATE_KEY']):
        credentials_dict = {
            'type': 'service_account',
            'client_id': os.environ['GCLOUD_CLIENT_ID'],
            'client_email': os.environ['GCLOUD_CLIENT_EMAIL'],
            'private_key_id': os.environ['GCLOUD_PRIVATE_KEY_ID'],
            'private_key': os.environ['GCLOUD_PRIVATE_KEY'].replace(r'\n', '\n'),
        }
        config['credentials_dict'] = credentials_dict
    else:
        raise Exception("Please set your Google Cloud service account details")

    if 'COMET_ML_TOKEN' in os.environ:
        COMET_ML_TOKEN = os.environ['COMET_ML_TOKEN']
        config['COMET_ML_TOKEN'] = COMET_ML_TOKEN

    return config

def upload_to_gcloud(location, filename, config):
    credentials = ServiceAccountCredentials.from_json_keyfile_dict(
        config['credentials_dict']
    )
    client = storage.Client(credentials=credentials, project=os.environ['GCLOUD_PROJECT'])
    bucket = client.get_bucket(os.environ['GCLOUD_BUCKET'])
    blob = bucket.blob(filename)
    blob.upload_from_filename(os.path.join(location,filename))
    return
