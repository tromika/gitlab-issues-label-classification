import gitlab
import pandas as pd
import pickle
import os, sys
import numpy as np
import gc
import copy
import logging

from comet_ml import Experiment

from sklearn.preprocessing import LabelBinarizer, LabelEncoder
from sklearn.model_selection import train_test_split

from keras.preprocessing import text, sequence
from keras import utils

import nltk
from nltk.tokenize import ToktokTokenizer
import mistune
import re
from typeing import Dict, Any

import spacy
from spacy.lang.en.stop_words import STOP_WORDS
nlp = spacy.load('en_core_web_sm')
word_split = re.compile('[^a-zA-Z0-9_\\+\\-/]')
cleanHTML = re.compile('<.*?>')

# After checking the first result these should be removed as well
CUSTOM_WORDS = {'ok', 'head', 'ago', 'author1', 'n', 'days', 'yes', 'no', 'hours', 'description', 'summary'}

def remove_html_tags(text):
    return re.sub(cleanHTML, '', text)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)

def cleanText(x: str) -> str:
    # Strip markdown
    text = remove_html_tags(mistune.markdown(x))
    filtered_words = [word.strip().lower() for word in word_split.split(text) if word not in STOP_WORDS.union(CUSTOM_WORDS)]
    return re.sub(' +',' '," ".join(filtered_words))


def pandas_explode(df: pd.DataFrame,
                   column_to_explode: str) -> pd.DataFrame:
    """
    Similar to Hive's EXPLODE function, take a column with iterable elements, and flatten the iterable to one element
    per observation in the output table

    :param df: A dataframe to explod
    :type df: pandas.DataFrame
    :param column_to_explode:
    :type column_to_explode: str
    :return: An exploded data frame
    :rtype: pandas.DataFrame
    """

    # Create a list of new observations
    new_observations = list()

    # Iterate through existing observations
    for row in df.to_dict(orient='records'):

        # Take out the exploding iterable
        explode_values = row[column_to_explode]
        del row[column_to_explode]

        # Create a new observation for every entry in the exploding iterable & add all of the other columns
        for explode_value in explode_values:

            # Deep copy existing observation
            new_observation = copy.deepcopy(row)

            # Add one (newly flattened) value from exploding iterable
            new_observation[column_to_explode] = explode_value

            # Add to the list of new observations
            new_observations.append(new_observation)

    # Create a DataFrame
    return_df = pd.DataFrame(new_observations)

    # Return
    return return_df

class Features:
    def __init__(self, config, experiment):
        self.config = config
        self.tokenize = None
        self.encoder = None
        # Number of words used to train the model
        self.max_words = 0
        # Number of lables
        self.num_classes = 0
        self.experiment = experiment

    def build_features(self, df_issues: pd.DataFrame) -> np.ndarray:
        df_issues['issue_text'] = (df_issues['title'].astype(str) + ' ' + df_issues['desc']).map(cleanText)
        X = self.tokenize.texts_to_matrix(df_issues['issue_text'])
        return X

    def save_tokenizer(self, tokenize : text.Tokenizer):
        self.tokenize = tokenize
        path = os.path.join("models","vocabulary")
        filename = "tokenizer_"+str(self.experiment.get_key())+".pkl"
        with open(os.path.join(path, filename), 'wb') as file:
            pickle.dump(tokenize, file, protocol=pickle.HIGHEST_PROTOCOL)

    def save_encoder(self, encoder : LabelBinarizer):
        self.encoder = encoder
        path = os.path.join("models","encoder")
        filename = "encoder_"+str(self.experiment.get_key())+".pkl"
        with open(os.path.join(path, filename), 'wb') as file:
            pickle.dump(encoder, file, protocol=pickle.HIGHEST_PROTOCOL)

    def get_train_test_data(self) -> Dict[str, pd.DataFrame]:
        logger.info("Load dataset")
        df_issues = pd.read_pickle(os.path.join("data","cache", "train.pkl"))
        df_train = pandas_explode(df_issues, 'tags')
        del(df_issues);
        gc.collect();

        # TODO move it to outside of the code
        use_labels = ['feature proposal', 'bug', 'frontend','Create',
                      'Platform', 'Plan', 'CI/CD','UX',  'backend', 'customer',
                      'regression', 'api', 'performance' ,'Geo', 'database',
                      'UI polish', 'devops:create', 'Manage', 'Distribution',
                      'import', 'Monitoring','potential proposal', 'Release',
                      'Packaging','security','customer+', 'authentication',
                      'admin dashboard', 'Security Products','container registry',
                      'webhooks', 'devops:release', 'Quality', 'boards',
                      'Gitaly','ci-build', 'kubernetes','oos', 'Secure',
                      'installation', 'Configuration', 'Monitoring']


        self.experiment.log_parameter('use_labels',use_labels)

        df_train = df_train[df_train.tags.isin(use_labels)]
        #Check how many unique words are in the issues
        x = nltk.FreqDist(ToktokTokenizer().tokenize(" ".join((df_train['title'].astype(str) + ' ' + df_train['desc']).values)))
        # Create train and validation datasets
        desc_train, desc_test, tags_train, tags_test = train_test_split(df_train[['title', 'desc']],
                                                                        df_train['tags'],
                                                                         test_size = 0.15,
                                                                          random_state = 123)

        logger.info('Gitlab issues has over {} unique words '.format(len(x)))
        del(df_train);

        # As a first trial let's use 40% of the vocab
        # Due to memory error let's use 20% of the vocab
        # Dropping out some labels resulted in I can use 30% of the vocab
        self.max_words = int(np.round(len(x) * 0.30))
        self.experiment.log_parameter('max_words',
                                 self.max_words)
        tokenize = text.Tokenizer(num_words=self.max_words,
                                  char_level=False)

        del(x);
        gc.collect();

        # Encoding
        encoder = LabelBinarizer()
        encoder.fit(tags_train)
        # Make sure we use only labels from the train dataset
        filter_tags = np.in1d(tags_test, tags_train.unique())

        Y_train = encoder.transform(tags_train)
        Y_test = encoder.transform(tags_test[filter_tags])

        self.num_classes = len(tags_train.unique())
        #Build internal vocabulary
        logger.info("Build internal vocabulary")
        tokenize.fit_on_texts(desc_train)
        self.save_tokenizer(tokenize)
        self.save_encoder(encoder)

        X_train = self.build_features(desc_train)
        X_test = self.build_features(desc_test[filter_tags])

        logger.info('X_train shape: {}'.format(X_train.shape))
        logger.info('X_test shape: {}'.format( X_test.shape))
        logger.info('Y_train shape: {}'.format( Y_train.shape))
        logger.info('Y_test shape: {}'.format( Y_test.shape))

        self.experiment.log_dataset_hash(X_train)
        return {'X_train' : X_train,
                'Y_train': Y_train,
                'X_test': X_test,
                'Y_test' : Y_test}
