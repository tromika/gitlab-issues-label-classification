import flask
import io
import os, sys
from keras.models import load_model
import pickle
import gitlab

import spacy
from spacy.lang.en.stop_words import STOP_WORDS
from bs4 import BeautifulSoup
from markdown import markdown
import re
import numpy as np
import pandas as pd
from gcloud import storage
from oauth2client.service_account import ServiceAccountCredentials
import tensorflow as tf
import warnings

import logging
from logging.handlers import RotatingFileHandler

app = flask.Flask(__name__)
model = None
tokenize = None
encoder = None
graph = None
CUSTOM_WORDS = {'ok', 'head', 'ago', 'author1', 'n', 'days', 'yes', 'no', 'hours', 'description', 'summary'}
nlp = spacy.load('en_core_web_sm')
word_split = re.compile('[^a-zA-Z0-9_\\+\\-/]')
warnings.filterwarnings("ignore", category=UserWarning, module='bs4')

def _connect_api():
    '''
    Returns:
        gitlab.Gitlab: API connection object

    '''
    if 'GITLAB_TOKEN' in os.environ:
        GITLAB_TOKEN = os.environ['GITLAB_TOKEN']
        gl = gitlab.Gitlab('http://gitlab.com',
                           private_token = GITLAB_TOKEN)
        gl.auth()
    else:
        raise Exception("Please set your Gitlab instance private token. https://gitlab.com/profile/personal_access_tokens")
    return gl


def load_keras_model(experiment):
    global model
    global encoder
    global tokenize
    global graph

    filename = 'keras_{}'.format(experiment)+'.h5'
    file = os.path.join('model', filename)
    if not os.path.isfile(file):
        download_from_gcloud(filename)
    model = load_model(file)
    # We have to make the graph global to make accessible from any thread
    graph = tf.get_default_graph()
    filename = 'tokenizer_{}'.format(experiment)+'.pkl'
    file = os.path.join('model', filename)
    if not os.path.isfile(file):
        download_from_gcloud(filename)
    tokenize = pickle.load( open( file, "rb" ) )
    filename = 'encoder_{}'.format(experiment)+'.pkl'
    file = os.path.join('model', filename)
    if not os.path.isfile(file):
        download_from_gcloud(filename)
    encoder = pickle.load( open( file, "rb" ) )

def cleanText(x):
    html = markdown(x)
    text = ''.join(BeautifulSoup(html).findAll(text=True));
    filtered_words = [word.strip().lower() for word in word_split.split(text) if word not in STOP_WORDS.union(CUSTOM_WORDS)]
    return re.sub(' +',' '," ".join(filtered_words))

def get_issue(project_id, issue_id):
    '''
    Returns:
            gitlab.Gitlab.issue: Gitlab issue details
    '''
    gl = _connect_api()
    project = gl.projects.get(project_id)
    return project.issues.get(issue_id)

def download_from_gcloud(filename):
    '''
    Download the model from Google Storage if needed
    Bit hardcoded even now
    '''
    if all( var in os.environ for var in ['GCLOUD_CLIENT_ID', 'GCLOUD_CLIENT_EMAIL', 'GCLOUD_PRIVATE_KEY_ID', 'GCLOUD_PRIVATE_KEY']):
        credentials_dict = {
            'type': 'service_account',
            'client_id': os.environ['GCLOUD_CLIENT_ID'],
            'client_email': os.environ['GCLOUD_CLIENT_EMAIL'],
            'private_key_id': os.environ['GCLOUD_PRIVATE_KEY_ID'],
            'private_key': os.environ['GCLOUD_PRIVATE_KEY'].replace(r'\n', '\n'),
        }
    else:
        raise Exception("Please set your Google Cloud service account details")
    credentials = ServiceAccountCredentials.from_json_keyfile_dict(
        credentials_dict
    )
    client = storage.Client(credentials=credentials, project=os.environ['GCLOUD_PROJECT'])
    bucket = client.get_bucket(os.environ['GCLOUD_BUCKET'])
    blob = bucket.blob(filename)
    location = 'model'
    blob.download_to_filename(os.path.join(location,filename))
    return


@app.route("/predict", methods=["GET"])
def predict():
    """
    This function responds to a request for /predict
    with the list of labels predicted for the given issue
    Please note only the first 3 labels are returned with
    the highest probability

    :return:        list of labels with the correspondig probability
    """
    global graph
    data = {"success": False}
    if flask.request.method == "GET":
        project_id = flask.request.args.get("project")
        issue_id = flask.request.args.get("issue")
        # Bit ugly but let's validate the project and the issue
        if int(project_id) > 0 and  int(issue_id) > 0:
            # Data prep
            issue = get_issue(project_id, issue_id)
            app.logger.info('Making prediction for project: {} issue: {}'.format(project_id, project_id))
            df_issues = pd.DataFrame(columns=["title", "desc"],
                                     data=[[issue.title,issue.description]])
            df_issues['issue_text'] = (df_issues['title'].astype(str) + ' ' + df_issues['desc']).map(cleanText)

            X = tokenize.texts_to_matrix(df_issues['issue_text'])
            with graph.as_default():
                prediction = model.predict(np.array([X][0]))
            text_labels = encoder.classes_
            predicted_label = text_labels[prediction.flatten().argsort()[-3:][::-1]]
            if predicted_label.shape[0] > 0:
                # Zip predicted labels and probability
                data["labels"] = dict(zip(predicted_label,
                                          prediction.flatten()[prediction.flatten().argsort()[-3:][::-1]].astype(str)))
                app.logger.info('Prediction was successful for project: {} issue: {}'.format(project_id, project_id))
                data["model_used_for_prediction"] = experiment
                # indicate that the request was a success
                data["success"] = True
    # return the data dictionary as a JSON response
    return flask.jsonify(data)


if __name__ == "__main__":
    handler = RotatingFileHandler('label_classifier.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)

    print(("* Loading Keras model and Flask starting server..."
        "please wait until server has fully started"))
    app.logger.info('Starting server')
    if 'EXPERIMENT' in os.environ:
        experiment = os.environ['EXPERIMENT']
    else:
        raise Exception("Please set an experiment as a model to use for predictions. You can find all the experiments on https://www.comet.ml/metricbrew/gitlab-issue-label-cf")
    # We have to load the keras model in advance -- this could take a while
    app.logger.info('Using {} for predictions'.format(experiment))
    load_keras_model(experiment)
    app.run(host='0.0.0.0')
