from tensorflow/tensorflow:1.10.1-py3
MAINTAINER Tamas Szuromi "tamas@metricbrew.com"

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python && \
    apt-get update && apt-get install -y --no-install-recommends \
      bzip2 \
      g++ \
      git \
      graphviz \
      libgl1-mesa-glx \
      libhdf5-dev \
      openmpi-bin \
      wget && \
    rm -rf /var/lib/apt/lists/*


RUN git clone https://gitlab.com/tromika/gitlab-issues-label-classification
WORKDIR gitlab-issues-label-classification/

RUN poetry config settings.virtualenvs.create false  && \
    poetry config settings.virtualenvs.in-project true  && \
    poetry install && \
    python -m spacy download en_core_web_sm

ENTRYPOINT ["python"]
