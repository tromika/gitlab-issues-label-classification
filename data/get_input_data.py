import gitlab
import pandas as pd
import os, logging, sys
import numpy as np


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                     level=logging.INFO, stream=sys.stdout)


def gitlab_api():
    '''
    Returns:
        gitlab.Gitlab: API connection object

    '''
    if 'GITLAB_TOKEN' in os.environ:
        GITLAB_TOKEN = os.environ['GITLAB_TOKEN']
        gl = gitlab.Gitlab('http://gitlab.com',
                           private_token = GITLAB_TOKEN)
        gl.auth()
    else:
        raise Exception("Please set your Gitlab instance private token. https://gitlab.com/profile/personal_access_tokens")
    return gl

class InputData:
    def __init__(self, config, projects_list: [int]):
        self.config = config
        self.project_list = project_list

    def download_issues(self) -> pd.DataFrame:
        '''
        Download Gitlab issues via https://docs.gitlab.com/ee/api/issues.html

        Returns Pandas df from the issue data
        '''
        gl = gitlab_api()
        # Project ids for GitLab Community Edition and GitLab Enterprise Edition
        projects = [gl.projects.get(p) for p in self.projects_list]
        all_issues = []
        logger.info("Downloading issues")
        for project in projects:
            issue_list = project.issues.list(all = True)
            all_issues.append(issue_list)

        df_issues = pd.DataFrame(columns=['id',
                                          'desc',
                                          'title',
                                          'tags',
                                          'project'])
        logger.info("Creating issues dataframe")
        for issues in all_issues:
            for issue in issues:
                df_issues = df_issues.append({'id': issue.id,
                               'desc' : issue.description,
                               'title' : issue.title,
                               'tags' : issue.labels,
                               'project' : issue.project_id}, ignore_index=True)

        return df_issues


    def download_input_data(self, save : bool = True) -> pd.DataFrame:
        df_issues = self.download_issues()
        if save:
            logger.info("Cache issues dataset as train.pkl")
            file_name = "train.pkl"
            issue_file = os.path.join("data","cache", file_name)
            df_issues.to_pickle(issue_file)
        return df_issues
